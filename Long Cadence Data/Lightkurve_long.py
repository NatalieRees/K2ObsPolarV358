# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
from lightkurve import KeplerTargetPixelFile
import os

homedir = "/Users/Natalie/Documents/AstroIII/Project/Long Cadence Data"
os.chdir(homedir)

tpf= KeplerTargetPixelFile('ktwo246179171-c19_lpd-targ.fits')
tpf.plot(frame=100)

#Create light curve
lc = tpf.to_lightcurve(aperture_mask=tpf.pipeline_mask)
lc.plot()