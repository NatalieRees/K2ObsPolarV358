#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  5 11:52:01 2019

@author: Natalie
"""
from astropy.io import fits
import pylab as plt

slc = fits.open('ktwo246179171-c19_slc.fits')
print (slc[0].header)
print (slc[1].columns)
t = slc[1].data['TIME']
f = slc[1].data['SAP_FLUX']
slc.close()

plt.plot(t,f,'-')
plt.show()
