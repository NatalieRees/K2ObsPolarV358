#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 25 16:48:24 2020

@author: Natalie
"""


from lightkurve import KeplerTargetPixelFile
import lightkurve
import os
import matplotlib.pyplot as plt
import numpy as np
import astropy.units as u
from scipy import optimize
import matplotlib
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)
from tabulate import tabulate

matplotlib.rcParams['font.sans-serif'] = "Calibri"
matplotlib.rcParams['font.family'] = "sans-serif"

def sin_func(x,a,b,c,d):
    return a*np.sin(b*x+c)+d 

def time_to_frame(TPF,times):
    no = np.argmin(abs(TPF.time-times))
    return no

def frame_to_time(TPF,frames):
    return TPF.time[frames]

def cycle_no(time):
    return ((time-2455532.66718)/0.1454497575).round()

def Ephem(E):
    return 2455532.66718 + E*0.1454497575

def Beuermann(E):
    return 2455532.66768 + E*0.145449767

def rms(data):
    mean = np.mean(data)
    return np.sqrt(np.sum((data-mean)**2)/len(data))


qual = lightkurve.utils.KeplerQualityFlags()

homedir = "/Users/Natalie/Documents/AstroIII/Project/Short Cadence Data"
os.chdir(homedir)

#----------Including all data-----------
tpf_all = KeplerTargetPixelFile('ktwo246179171-c19_spd-targ.fits',quality_bitmask =65536)
lc_all = tpf_all.to_lightcurve(aperture_mask = tpf_all.pipeline_mask)

#----------Excluding default and no fine pointing flags--------
tpf= KeplerTargetPixelFile('ktwo246179171-c19_spd-targ.fits',quality_bitmask =1130799+32768)

qual.create_quality_mask(tpf.quality,1130799)

#Build new aperture
aper = np.zeros(tpf.shape[1:])
#aper[:]=1
aper[0:4,2:5]=1
aper_reverse = np.zeros(tpf.shape[1:])
aper_reverse[aper==0]=1
#tpf.plot(aperture_mask=aper, mask_color='red')

#Create original light curve
lc_og = tpf.to_lightcurve(aperture_mask=tpf.pipeline_mask)
#lc_og.plot()

#Create custom light curve
lc_custom = tpf.to_lightcurve(aperture_mask = aper.astype(bool)).remove_nans()
#lc_custom.scatter()

#Create reverse light curve
lc_reverse = tpf.to_lightcurve(aperture_mask = aper_reverse.astype(bool)).remove_nans()
#lc_reverse.scatter()

#Noisy frames to remove
noisy = [100,267,1302,1829,1836,1845,1952,2180,2389,2528,2635,2973,3034,3144,3343,3600,
         3901,4148,4240,4839,6116,6576,6744,6943,7202,7671,9108,9258,9471,
         9637,9971,10171,10452,12331,13324,13409,13772,13775,14304,14390,
         14981,15060,15142]

lc_cleaned = lc_custom[0:100]

for i in range (0,len(noisy)-1):
    lc_cleaned = lc_cleaned.append(lc_custom[noisy[i]+1:noisy[i+1]])

lc_cleaned = lc_cleaned.append(lc_custom[15143:len(lc_custom)])    

lc_cleaned.plot()

#Create periodgram
pg_all = lc_cleaned.to_periodogram(oversample_factor=1)
#pg_all.plot(view='period', scale='log')

#Fold on orbital period
pg = lc_cleaned.to_periodogram(minimum_period = 0.14*u.day,maximum_period = 0.15*u.day,oversample_factor=10)
period = float(str(pg.period_at_max_power)[0:20])

#Second peak in periodogram
pg_second = lc_cleaned.to_periodogram(minimum_period = 0.07*u.day,maximum_period = 0.08*u.day,oversample_factor=10)
half_period = float(str(pg_second.period_at_max_power)[0:19])

#Section of the light curve
n=70
centre = 3536.518 + n*period
start = centre-0.02
end = centre+0.02
flux_section = lc_cleaned.flux[time_to_frame(lc_cleaned,start):time_to_frame(lc_cleaned,end)]
time_section = lc_cleaned.time[time_to_frame(lc_cleaned,start):time_to_frame(lc_cleaned,end)]
fig, ax = plt.subplots()
ax.plot(time_section,flux_section)
c = 3*np.pi/2 - 2*np.pi*time_section[np.argmin(flux_section)]/half_period
params_init = [150,2*np.pi/half_period,c,400]
params,params_covariance = optimize.curve_fit(sin_func,time_section,flux_section,p0 = [params_init])
ax.plot(time_section,sin_func(time_section,*params),label = 'P = '+str(2*np.pi/params[1])+'d')
ax.legend()
ax.set(xlabel = 'Time - 2454833 [BKJD days]',ylabel = 'Flux [$e^-s^{-1}$]') 
t_min = (3*np.pi/2 - params[2])/params[1]
print(t_min)

minima_times = []
n_values = list(np.arange(50))+list(np.arange(59,64))+list(np.arange(66,71))+list(np.arange(72,77))+list(np.arange(96,102))
for n in n_values:
    print(n)
    centre = 3536.518 + n*period
    start = centre-0.02
    end = centre+0.02
    flux_section = lc_cleaned.flux[time_to_frame(lc_cleaned,start):time_to_frame(lc_cleaned,end)]
    time_section = lc_cleaned.time[time_to_frame(lc_cleaned,start):time_to_frame(lc_cleaned,end)]
    fig, ax = plt.subplots()
    ax.plot(time_section,flux_section)
    c = 3*np.pi/2 - 2*np.pi*time_section[np.argmin(flux_section)]/half_period
    params_init = [150,2*np.pi/half_period,c,400]
    params,params_covariance = optimize.curve_fit(sin_func,time_section,flux_section,p0 = [params_init])
    ax.plot(time_section,sin_func(time_section,*params),label = 'P = '+str(2*np.pi/params[1])+'d')
    ax.legend()
    ax.set(xlabel = 'Time - 2454833 [BKJD days]',ylabel = 'Flux [$e^-s^{-1}$]') 
    t_min = (3*np.pi/2 - params[2])/params[1]
    #print(t_min)
    minima_times +=[t_min]

K_minima = np.array(minima_times)+2454833.0

B_minima = np.array([50050.66540,51079.72542,51080.74122,51080.74008,
            51080.74028,51081.61349,51081.61558,51081.61560,51081.61634,
            51081.75574,51173.24726,53215.79740,53286.63117,
            53287.64619,53288.51900,55532.67080,55534.56111,55559.57386,
            55863.71032,56911.82466,56945.85807,56946.87451,56954.73144,
            56971.74748,57558.63726,57573.61675])+2400000

Tmin = np.append(K_minima,B_minima)

E_values = cycle_no(Tmin)

[m,t0],params_cov = np.polyfit(E_values,Tmin,1,full =False,cov=True)

fig,ax = plt.subplots()
ax.scatter(Tmin,E_values,marker = '+',s=2,c='black')
ax.plot(Beuermann(E_values),E_values,lw = 1,label = 'Beuermann')
ax.plot(Ephem(E_values),E_values,lw = 1,label = 'Updated')
ax.legend()

fig,ax = plt.subplots()
ax.scatter(K_minima,cycle_no(K_minima),s=1,c='black')
ax.plot(Beuermann(cycle_no(K_minima)),cycle_no(K_minima),lw = 0.5,label = 'Beuermann')
ax.plot(Ephem(cycle_no(K_minima)),cycle_no(K_minima),lw = 0.5,label = 'Updated')
ax.legend()
ax.set(xlabel ='Minima Time',ylabel = 'Cycle Number')

fig,ax = plt.subplots()
ax.scatter(cycle_no(K_minima),K_minima-Ephem(cycle_no(K_minima)))
ax.scatter(cycle_no(K_minima),K_minima-Beuermann(cycle_no(K_minima)))

fig,ax = plt.subplots()
ax.scatter(E_values,Tmin-Ephem(E_values))
ax.scatter(E_values,Tmin-Beuermann(E_values))

KOC = np.round((K_minima-Ephem(cycle_no(K_minima)))*24*60,2)
BOC = np.array([-14.82,-0.81,3.39,0.01,-1.63,-1.34,-0.60,2.40,2.43,3.50,-5.21,-0.00, 
                -10.31,-1.35,-1.73,-6.24,-6.07,4.50,3.73,-2.91,-1.47,3.31,0.67,-1.79,
                2.01,-0.28,-0.31,-2.96])
BOC2 = np.array([-0.81,3.39,0.01,-1.63,-1.34,-0.60,2.40,2.43,3.50,-5.21,-0.00,
                 -1.35,-1.73,-6.24,-6.07,4.50,3.73,-2.91,-1.47,3.31,0.67,-1.79,
                2.01,-0.28,-0.31,-2.96])   

#Plot sections as a function of phase
start = 3539.27
end = 3539.45
phase0 = minima_times[np.argmin(abs(start-np.array(minima_times)))]
lc_sec = lc_cleaned[time_to_frame(lc_cleaned,start):time_to_frame(lc_cleaned,end)]
phases = (lc_sec.time-phase0)/m
fig, ax = plt.subplots()
ax.plot(phases,lc_sec.flux)
ax.yaxis.set_minor_locator(AutoMinorLocator())
ax.xaxis.set_minor_locator(AutoMinorLocator())
ax.set_xlabel( 'Phase, $\phi_{orb}$',fontsize = 14)
ax.set_ylabel( 'Flux [$e^-s^{-1}$]',fontsize=14)
ax.set_xlim(-0.2,1.2)

