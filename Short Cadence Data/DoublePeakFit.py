#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 25 10:26:50 2020

@author: Natalie
"""

from lightkurve import KeplerTargetPixelFile
import lightkurve
import os
import matplotlib.pyplot as plt
import numpy as np
import astropy.units as u
from scipy import optimize
import matplotlib

matplotlib.rcParams['font.sans-serif'] = "Calibri"
matplotlib.rcParams['font.family'] = "sans-serif"


def double_sin_func(x,a,b,c,d,e,f):
    w = 2*np.pi/0.14543843321313646
    return a*np.sin(b*x+c)+d + e*np.sin(w*x+f)

def time_to_frame(TPF,times):
    no = np.argmin(abs(TPF.time-times))
    return no

def frame_to_time(TPF,frames):
    return TPF.time[frames]


qual = lightkurve.utils.KeplerQualityFlags()

homedir = "/Users/Natalie/Documents/AstroIII/Project/Short Cadence Data"
os.chdir(homedir)

#----------Including all data-----------
tpf_all = KeplerTargetPixelFile('ktwo246179171-c19_spd-targ.fits',quality_bitmask =65536)
lc_all = tpf_all.to_lightcurve(aperture_mask = tpf_all.pipeline_mask)

#----------Excluding default and no fine pointing flags--------
tpf= KeplerTargetPixelFile('ktwo246179171-c19_spd-targ.fits',quality_bitmask =1130799+32768)

qual.create_quality_mask(tpf.quality,1130799)

#Build new aperture
aper = np.zeros(tpf.shape[1:])
#aper[:]=1
aper[0:4,2:5]=1
aper_reverse = np.zeros(tpf.shape[1:])
aper_reverse[aper==0]=1
#tpf.plot(aperture_mask=aper, mask_color='red')

#Create original light curve
lc_og = tpf.to_lightcurve(aperture_mask=tpf.pipeline_mask)
#lc_og.plot()

#Create custom light curve
lc_custom = tpf.to_lightcurve(aperture_mask = aper.astype(bool)).remove_nans()
#lc_custom.scatter()

#Create reverse light curve
lc_reverse = tpf.to_lightcurve(aperture_mask = aper_reverse.astype(bool)).remove_nans()
#lc_reverse.scatter()

#Noisy frames to remove
noisy = [100,267,1302,1829,1836,1845,1952,2180,2389,2528,2635,2973,3034,3144,3343,3600,
         3901,4148,4240,4839,6116,6576,6744,6943,7202,7671,9108,9258,9471,
         9637,9971,10171,10452,12331,13324,13409,13772,13775,14304,14390,
         14981,15060,15142]

lc_cleaned = lc_custom[0:100]

for i in range (0,len(noisy)-1):
    lc_cleaned = lc_cleaned.append(lc_custom[noisy[i]+1:noisy[i+1]])

lc_cleaned = lc_cleaned.append(lc_custom[15143:len(lc_custom)])    

lc_cleaned.plot()

#Create periodgram
pg_all = lc_cleaned.to_periodogram(oversample_factor=1)
#pg_all.plot(view='period', scale='log')

#Fold on orbital period
pg = lc_cleaned.to_periodogram(minimum_period = 0.14*u.day,maximum_period = 0.15*u.day,oversample_factor=10)
period = float(str(pg.period_at_max_power)[0:20])

#Second peak in periodogram
pg_second = lc_cleaned.to_periodogram(minimum_period = 0.07*u.day,maximum_period = 0.08*u.day,oversample_factor=10)
half_period = float(str(pg_second.period_at_max_power)[0:19])

#Section of the light curve
start = sections[5][0]
end = sections[5][1]
lc_sec = lc_cleaned[time_to_frame(lc_cleaned,start):time_to_frame(lc_cleaned,end)]
c = np.pi/2 - 2*np.pi*lc_sec.time[np.argmax(lc_sec.flux)]/half_period
params_init = [150,2*np.pi/half_period,c,400,150,c]
params,params_covariance = optimize.curve_fit(double_sin_func,lc_sec.time,lc_sec.flux,p0 = [params_init])

phase0 = minima_times[np.argmin(abs(start-np.array(minima_times)))]
fig, ax = plt.subplots()
ax.plot((lc_sec.time-phase0)/m,lc_sec.flux)
ax.plot((lc_sec.time-phase0)/m,double_sin_func(lc_sec.time,*params),label = 'P = '+str(2*np.pi/params[1])+'d')
ax.legend()
ax.set_xlabel('Phase, $\phi_{orb}$',fontsize= 14)
ax.set_ylabel ('Flux [$e^-s^{-1}$]',fontsize = 14) 
ax.yaxis.set_minor_locator(AutoMinorLocator())
ax.xaxis.set_minor_locator(AutoMinorLocator())
ax.set_xlim(-0.2,1.2)
error = 2*np.pi*params_covariance[1][1]**(1/2)/params[1]**2

sections = [[3539.26,3539.45],[3538.825,3539.015],[3537.665,3537.86],
            [3539.55,3539.75],[3547.11,3547.31],[3538.66,3538.9],[3537.94,3538.15]]