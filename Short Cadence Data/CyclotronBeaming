#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  7 13:05:32 2020

@author: Natalie
"""

import os
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)

def angle(i,b,phase):
    cos_theta = np.cos(i)*np.cos(b)-np.sin(i)*np.sin(b)*np.cos(np.pi*2*phase+np.pi)
    return np.arccos(cos_theta)*180/np.pi

matplotlib.rcParams['font.sans-serif'] = "Calibri"
matplotlib.rcParams['font.family'] = "sans-serif"

i = 58.3*np.pi/180
b = 15.0*np.pi/180
phase = np.arange(0,1.01,0.01)

theta = angle(i,b,phase)

AMi = 50*np.pi/180
AMb = 55*np.pi/180
AM_theta = angle(AMi,AMb,phase)
for i in np.where(AM_theta>90):
    AM_theta[i] = 180-AM_theta[i]

fig,ax = plt.subplots()
ax.plot(phase,AM_theta,linestyle = 'dashed',c = 'black',linewidth = 1,label = 'AM Her')
ax.plot(phase,theta,label = 'V358 Aqr')
ax.set_xlabel( 'Phase, $\phi_{orb}$',fontsize = 14)
ax.set_ylabel( r'$\theta [\degree$]',fontsize=14)
ax.yaxis.set_minor_locator(AutoMinorLocator())
ax.xaxis.set_minor_locator(AutoMinorLocator())
ax.set_xlim(0,1)
ax.set_ylim(0,100)
ax.legend()


