#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 11 16:25:26 2019

@author: Natalie
"""

from lightkurve import KeplerTargetPixelFile
import lightkurve
import os
import matplotlib.pyplot as plt
import numpy as np
import astropy.units as u
from scipy import optimize
from astropy.timeseries import LombScargle
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)
import matplotlib

matplotlib.rcParams['font.sans-serif'] = "Calibri"
matplotlib.rcParams['font.family'] = "sans-serif"

def time_to_frame(TPF,times):
    no = np.argmin(abs(TPF.time-times))
    return no

def frame_to_time(TPF,frames):
    return TPF.time[frames]

def plot_flare(LC,frame):
    fig, ax = plt.subplots()
    ax.plot((LC.time[frame-3:frame+10]-LC.time[frame])*24*3600,LC.flux[frame-3:frame+10])
    ax.set(title = 'Possible flare at t='+str(LC.time[frame])+'d')
    ax.yaxis.set_minor_locator(AutoMinorLocator())
    ax.xaxis.set_minor_locator(AutoMinorLocator())
    ax.set_xlabel ( 'Time/s', fontsize = 12)
    ax.set_ylabel ('Flux [$e^-s^{-1}$]', fontsize = 12)


def plot_flare_magnitudes(LC,frame):
    fig, ax = plt.subplots()
    flux = LC.flux[frame-4:frame+10]
    time = (LC.time[frame-4:frame+10]-LC.time[frame])*24*3600
    ax.plot(time,25.3 -2.5*np.log10(flux))
    ax.set_ylim(ax.get_ylim()[::-1])
    ax.set_xlabel ( 'Time/s', fontsize = 12)
    ax.set_ylabel ('Kepler Magnitudes, Kp', fontsize = 12)
    ax.yaxis.set_minor_locator(AutoMinorLocator())
    ax.xaxis.set_minor_locator(AutoMinorLocator())

def sin_func(x,a,b,c,d):
    return a*np.sin(b*x+c)+d

qual = lightkurve.utils.KeplerQualityFlags()

homedir = "/Users/Natalie/Documents/AstroIII/Project/Short Cadence Data"
os.chdir(homedir)

#----------Including all data-----------
tpf_all = KeplerTargetPixelFile('ktwo246179171-c19_spd-targ.fits',quality_bitmask =65536)
lc_all = tpf_all.to_lightcurve(aperture_mask = tpf_all.pipeline_mask)

#----------Excluding default and no fine pointing flags--------
tpf= KeplerTargetPixelFile('ktwo246179171-c19_spd-targ.fits',quality_bitmask =1130799+32768)

qual.create_quality_mask(tpf.quality,1130799)

#Build new aperture
aper = np.zeros(tpf.shape[1:])
#aper[:]=1
aper[0:4,2:5]=1
aper_reverse = np.zeros(tpf.shape[1:])
aper_reverse[aper==0]=1
ax = tpf.plot(frame = 0,aperture_mask=aper, mask_color='red')
ax.set_title("")

#Create original light curve
lc_og = tpf.to_lightcurve(aperture_mask=tpf.pipeline_mask)
#lc_og.plot()

#Create custom light curve
lc_custom = tpf.to_lightcurve(aperture_mask = aper.astype(bool)).remove_nans()
#lc_custom.scatter()

#Create reverse light curve
lc_reverse = tpf.to_lightcurve(aperture_mask = aper_reverse.astype(bool)).remove_nans()
#lc_reverse.scatter()

#Noisy frames to remove
noisy = [100,267,1302,1829,1836,1845,1952,2180,2389,2528,2635,2973,3034,3144,3343,3600,
         3901,4148,4240,4839,6116,6576,6744,6943,7202,7671,9108,9258,9471,
         9637,9971,10171,10452,12331,13324,13409,13772,13775,14304,14390,
         14981,15060,15142]

lc_cleaned = lc_custom[0:100]

for i in range (0,len(noisy)-1):
    lc_cleaned = lc_cleaned.append(lc_custom[noisy[i]+1:noisy[i+1]])

lc_cleaned = lc_cleaned.append(lc_custom[15143:len(lc_custom)])    

lc_cleaned.plot()

#Create periodgram
pg_all = lc_cleaned.to_periodogram(oversample_factor=1)
fig,ax = plt.subplots()
ax = pg_all.plot(view='period', scale='log')
ax.set_xlabel('Period [d]',fontsize = 14)
ax.set_ylabel('Power [$e^-s^{-1}$] ',fontsize = 14)
ax.get_legend().remove()

#Fold on orbital period
pg = lc_cleaned.to_periodogram(minimum_period = 0.14*u.day,maximum_period = 0.15*u.day,oversample_factor=10)
period = float(str(pg.period_at_max_power)[0:20])
print(period)
folded = lc_cleaned.fold(pg.period_at_max_power, 3536.517842437047)
for i in np.where(folded.phase<0)[0]:
    folded.phase[i] = folded.phase[i]+1
fig, ax = plt.subplots()
ax.scatter(folded.phase,folded.flux, marker='.',s=1, c='black')
ax.set_xlabel('Phase, $\phi_{orb}$',fontsize = 12)
ax.set_ylabel('Flux [$e^-s^{-1}$] ',fontsize = 12)
ax.set_xlim(0,1)
ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.yaxis.set_minor_locator(AutoMinorLocator())
ax.tick_params(which='minor', length=4, color='black')

#Folded with two orbits
folded = lc_cleaned.fold(0.1454497575, 2455532.66718-2454833.0)
for i in np.where(folded.phase<0)[0]:
    folded.phase[i] = folded.phase[i]+1
fig, ax = plt.subplots()
ax.scatter(np.append(folded.phase,folded.phase+1),np.append(folded.flux,folded.flux), marker='.',s=1, c='black')
ax.set_xlabel('Phase, $\phi_{orb}$',fontsize = 12)
ax.set_ylabel('Flux [$e^-s^{-1}$] ',fontsize = 12)
ax.set_xlim(0,2)
ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.yaxis.set_minor_locator(AutoMinorLocator())
ax.tick_params(which='minor', length=4, color='black')

#Second peak in periodogram
pg_second = lc_cleaned.to_periodogram(minimum_period = 0.07*u.day,maximum_period = 0.08*u.day,oversample_factor=10)
half_period = float(str(pg_second.period_at_max_power)[0:19])

#Folded light curve in Kepler magnitudes
fig, ax = plt.subplots()
ax.scatter(folded.phase,25.3-2.5*np.log10(folded.flux), marker='.',s=1)
ax.set_xlabel('Phase, $\phi_{orb}$',fontsize = 12)
ax.set_ylabel('Kepler Magnitudes, Kp',fontsize = 12)
ax.set_xlim(0,1)
ax.set_ylim(ax.get_ylim()[::-1])
ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.yaxis.set_minor_locator(AutoMinorLocator())
ax.tick_params(which='minor', length=4, color='black')


#Seperate the light curve into sections without gaps
lc1 = lc_cleaned[0:time_to_frame(lc_cleaned,3544.83)]
lc2 = lc_cleaned[time_to_frame(lc_cleaned,3544.83)+16:time_to_frame(lc_cleaned,3545.97)]
lc3 = lc_cleaned[time_to_frame(lc_cleaned,3545.985)-4:time_to_frame(lc_cleaned,3546.715)+3]
lc4 = lc_cleaned[time_to_frame(lc_cleaned,3546.96):time_to_frame(lc_cleaned,3547.69)+2]
lc5 = lc_cleaned[time_to_frame(lc_cleaned,3550.41)-4:]

#Study how the second peak changes with time
time = []
mean_flux = []
power_first = []
power_second = []
rel_power = []
for i in range (0,40):
    lc1_section = lc1[time_to_frame(lc1,lc1.time[0]+period*i):time_to_frame(lc1,lc1.time[0]+period*(i+10))]
   # lc1[time_to_frame(lc1,lc1.time[0]+period*i):time_to_frame(lc1,lc1.time[0]+period*(i+10))].plot()
   # lc1_section.to_periodogram(minimum_period = 0.03*u.day,maximum_period = 0.2*u.day,
   #         oversample_factor=1).plot(view='period', scale='log')
    pg_first = lc1_section.to_periodogram(minimum_period = 0.14*u.day,maximum_period = 0.15*u.day,oversample_factor=10)
    max_power_first = float(str(pg_first.max_power)[0:16])
    power_first += [max_power_first]
    pg_second = lc1_section.to_periodogram(minimum_period = 0.067*u.day,maximum_period = 0.08*u.day,oversample_factor=10)
    max_power_second = float(str(pg_second.max_power)[0:16])
    power_second += [max_power_second]
    mean_flux += [lc1_section.flux.sum()/len(lc1_section.flux)]
    time += [lc1.time[0]+period*(i+5)]
    rel_power += [max_power_second/max_power_first]
    
#Plot section of the light cuvre
start = 3537.67
end = 3539.01
lc_section1 = lc_cleaned[time_to_frame(lc_cleaned,start):time_to_frame(lc_cleaned,end)]
lc_section1.plot()

start = 3540.85
end = 3542.21
lc_section2 = lc_cleaned[time_to_frame(lc_cleaned,start):time_to_frame(lc_cleaned,end)]
lc_section2.plot()

#Light curve with colour coded sections

fig,ax = plt.subplots()
ax.plot(lc_custom.time,lc_custom.flux, c='black',lw=0.3)
ax.set_xlabel('Time [BKJD days]',fontsize = 12)
ax.set_ylabel('Flux [$e^-s^{-1}$]',fontsize = 12)
ax.set_xlim(lc_cleaned.time[0],lc_cleaned.time[-1])
ax.xaxis.set_minor_locator(AutoMinorLocator())
ax.yaxis.set_minor_locator(AutoMinorLocator())
ax.tick_params(which='minor', length=4, color='black')

fig,ax = plt.subplots()
ax.plot(lc_cleaned.time,lc_cleaned.flux, c='black',lw=0.3)
ax.plot(lc_section1.time,lc_section1.flux,c='blue',lw=0.3)
ax.plot(lc_section2.time,lc_section2.flux,c='red',lw=0.3)
ax.set_xlabel('Time [BKJD days]',fontsize = 12)
ax.set_ylabel('Flux [$e^-s^{-1}$]',fontsize = 12)
ax.set_xlim(lc_cleaned.time[0],lc_cleaned.time[-1])
ax.xaxis.set_minor_locator(AutoMinorLocator())
ax.yaxis.set_minor_locator(AutoMinorLocator())
ax.tick_params(which='minor', length=4, color='black')

fig,ax = plt.subplots()
ax.plot(lc_section1.time,lc_section1.flux,c='blue',lw=0.3)
ax.set_xlabel('Time [BKJD days]',fontsize = 12)
ax.set_ylabel('Flux [$e^-s^{-1}$]',fontsize = 12)
ax.set_xlim(lc_section1.time[0],lc_section1.time[-1])
ax.xaxis.set_minor_locator(AutoMinorLocator())
ax.yaxis.set_minor_locator(AutoMinorLocator())
ax.tick_params(which='minor', length=4, color='black')

fig,ax = plt.subplots()
ax.plot(lc_section2.time,lc_section2.flux,c='red',lw=0.3)
ax.set_xlabel('Time [BKJD days]',fontsize = 12)
ax.set_ylabel('Flux [$e^-s^{-1}$]',fontsize = 12)
ax.set_xlim(lc_section2.time[0],lc_section2.time[-1])
ax.xaxis.set_minor_locator(AutoMinorLocator())
ax.yaxis.set_minor_locator(AutoMinorLocator())
ax.tick_params(which='minor', length=4, color='black')


