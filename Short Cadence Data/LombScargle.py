#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 11:08:56 2020

@author: Natalie
"""


from lightkurve import KeplerTargetPixelFile
import lightkurve
import os
import matplotlib.pyplot as plt
import numpy as np
import astropy.units as u
from scipy import optimize
from astropy.timeseries import LombScargle
import matplotlib

matplotlib.rcParams['font.sans-serif'] = "Calibri"
matplotlib.rcParams['font.family'] = "sans-serif"

def time_to_frame(TPF,times):
    no = np.argmin(abs(TPF.time-times))
    return no

def frame_to_time(TPF,frames):
    return TPF.time[frames]

def plot_flare(LC,frame):
    fig, ax = plt.subplots()
    ax.plot((LC.time[frame-3:frame+10]-LC.time[frame])*24*3600,LC.flux[frame-3:frame+10])
    ax.set(title = 'Possible flare at t='+str(LC.time[frame])+'d',xlabel = 'time/s',ylabel='Flux [$e^-s^{-1}$]')

qual = lightkurve.utils.KeplerQualityFlags()

homedir = "/Users/Natalie/Documents/AstroIII/Project/Short Cadence Data"
os.chdir(homedir)

#----------Including all data-----------
tpf_all = KeplerTargetPixelFile('ktwo246179171-c19_spd-targ.fits',quality_bitmask =65536)
lc_all = tpf_all.to_lightcurve(aperture_mask = tpf_all.pipeline_mask)

#----------Excluding default and no fine pointing flags--------
tpf= KeplerTargetPixelFile('ktwo246179171-c19_spd-targ.fits',quality_bitmask =1130799+32768)

qual.create_quality_mask(tpf.quality,1130799)

#Build new aperture
aper = np.zeros(tpf.shape[1:])
#aper[:]=1
aper[0:4,2:5]=1
aper_reverse = np.zeros(tpf.shape[1:])
aper_reverse[aper==0]=1
tpf.plot(aperture_mask=aper, mask_color='red')

#Create original light curve
lc_og = tpf.to_lightcurve(aperture_mask=tpf.pipeline_mask)
#lc_og.plot()

#Create custom light curve
lc_custom = tpf.to_lightcurve(aperture_mask = aper.astype(bool)).remove_nans()
#lc_custom.scatter()

#Create reverse light curve
lc_reverse = tpf.to_lightcurve(aperture_mask = aper_reverse.astype(bool)).remove_nans()
#lc_reverse.scatter()

#Noisy frames to remove
noisy = [100,267,1302,1829,1836,1845,1952,2180,2389,2528,2635,2973,3034,3144,3343,3600,
         3901,4148,4240,4839,6116,6576,6744,6943,7202,7671,9108,9258,9471,
         9637,9971,10171,10452,12331,13324,13409,13772,13775,14304,14390,
         14981,15060,15142]

lc_cleaned = lc_custom[0:100]

for i in range (0,len(noisy)-1):
    lc_cleaned = lc_cleaned.append(lc_custom[noisy[i]+1:noisy[i+1]])

lc_cleaned = lc_cleaned.append(lc_custom[15143:len(lc_custom)])    

lc_cleaned.plot()

#Fold on orbital period
pg = lc_cleaned.to_periodogram(minimum_period = 0.14*u.day,maximum_period = 0.15*u.day,oversample_factor=10)
period = float(str(pg.period_at_max_power)[0:20])

#Seperate the light curve into sections without gaps
lc1 = lc_cleaned[0:time_to_frame(lc_cleaned,3544.83)]
#lc1.plot()
lc2 = lc_cleaned[time_to_frame(lc_cleaned,3544.83)+16:time_to_frame(lc_cleaned,3545.97)]
#lc2.plot()
lc3 = lc_cleaned[time_to_frame(lc_cleaned,3545.985)-4:time_to_frame(lc_cleaned,3546.715)+3]
#lc3.plot()
lc4 = lc_cleaned[time_to_frame(lc_cleaned,3546.96):time_to_frame(lc_cleaned,3547.69)+2]
#lc4.plot()
lc5 = lc_cleaned[time_to_frame(lc_cleaned,3550.41)-4:]
#lc5.plot()

freq  = np.linspace(0.1, 50.1, 1000)
times = []
ampl_array = []
for i in range (0,46):
    lc1_section = lc1[time_to_frame(lc1,lc1.time[0]+period*i):time_to_frame(lc1,lc1.time[0]+period*(i+5))]
    power = LombScargle(lc1_section.time,lc1_section.flux, normalization='psd').power(freq)
    ampl  = list(np.sqrt(power))
    ampl_array += [ampl]
    times += [lc1_section.time[0]+period*(2.5)]
    

fig,ax = plt.subplots()
im = ax.imshow(np.flip(np.array(ampl_array).transpose()),aspect = 'auto',extent = [times[0],times[-1],0,50])
plt.colorbar(im)
ax.set_ylabel ('Frequency [cycles/d]', fontsize = 14)
ax.set_xlabel ('Time - 2454833 [BKJD days]',fontsize=14)
