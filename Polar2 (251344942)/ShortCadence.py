#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  1 16:13:06 2020

@author: Natalie
"""


from lightkurve import KeplerTargetPixelFile
import lightkurve
import os
import matplotlib.pyplot as plt
import numpy as np
import astropy.units as u

def time_to_frame(TPF,times):
    no = np.argmin(abs(TPF.time-times))
    return no

def frame_to_time(TPF,frames):
    return TPF.time[frames]

def plot_flare(LC,frame):
    fig, ax = plt.subplots()
    ax.plot((LC.time[frame-3:frame+10]-LC.time[frame])*24*3600,LC.flux[frame-3:frame+10])
    ax.set(title = 'Possible flare at t='+str(LC.time[frame])+'d',xlabel = 'time/s',ylabel='Flux [$e^-s^{-1}$]')

qual = lightkurve.utils.KeplerQualityFlags()

homedir = "/Users/Natalie/Documents/AstroIII/Project/Polar2 (251344942)"
os.chdir(homedir)

#----------Including all data-----------
tpf_all = KeplerTargetPixelFile('ktwo251344942-c16_spd-targ.fits',quality_bitmask =65536)
lc_all = tpf_all.to_lightcurve(aperture_mask = tpf_all.pipeline_mask)

#----------Excluding default and no fine pointing flags--------
tpf= KeplerTargetPixelFile('ktwo251344942-c16_spd-targ.fits',quality_bitmask =1130799+32768)

qual.create_quality_mask(tpf.quality,1130799)

#Build new aperture
aper = np.zeros(tpf.shape[1:])
aper[0:4,2:5]=1
aper_reverse = np.zeros(tpf.shape[1:])
aper_reverse[aper==0]=1
#tpf.plot(aperture_mask=aper, mask_color='red')

#Create original light curve
tpf.plot(aperture_mask=tpf.pipeline_mask, mask_color='red')
lc_og = tpf.to_lightcurve(aperture_mask=tpf.pipeline_mask)
lc_og.plot()

#Create custom light curve
lc_custom = tpf.to_lightcurve(aperture_mask = aper.astype(bool)).remove_nans()
#lc_custom.scatter()


#Noisy frames to remove
noisy = [100,267,1302,1952,2180,2389,2528,2635,2973,3034,3144,3343,3600,
         3901,4148,4240,4839,6116,6576,6744,6943,7202,7671,9108,9258,9471,
         9637,9971,10171,10452,12331,13324,13409,13772,13775,14304,14390,
         14981,15060,15142]

lc_cleaned = lc_custom[0:100]

for i in range (0,len(noisy)-1):
    lc_cleaned = lc_cleaned.append(lc_custom[noisy[i]+1:noisy[i+1]])

lc_cleaned = lc_cleaned.append(lc_custom[15143:len(lc_custom)])    

#lc_cleaned.plot()

#Create periodgram
pg = lc_og.to_periodogram(oversample_factor=1)
pg.plot(view='period', scale='log')
pg.plot(view='period')
lc_og.fold(pg.period_at_max_power).scatter()
