#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  1 17:36:18 2020

@author: Natalie
"""


from lightkurve import KeplerTargetPixelFile
import lightkurve
import os
import matplotlib.pyplot as plt
import numpy as np
import astropy.units as u

def time_to_frame(TPF,times):
    no = np.argmin(abs(TPF.time-times))
    return no

def frame_to_time(TPF,frames):
    return TPF.time[frames]

def plot_flare(LC,frame):
    fig, ax = plt.subplots()
    ax.plot((LC.time[frame-3:frame+10]-LC.time[frame])*24*3600,LC.flux[frame-3:frame+10])
    ax.set(title = 'Possible flare at t='+str(LC.time[frame])+'d',xlabel = 'time/s',ylabel='Flux [$e^-s^{-1}$]')

qual = lightkurve.utils.KeplerQualityFlags()

homedir = "/Users/Natalie/Documents/AstroIII/Project/Polar3(246864600)"
os.chdir(homedir)

#----------Including all data-----------
tpf_all = KeplerTargetPixelFile('ktwo246864600-c13_lpd-targ.fits',quality_bitmask =65536)
lc_all = tpf_all.to_lightcurve(aperture_mask = tpf_all.pipeline_mask)

#----------Excluding default and no fine pointing flags--------
tpf= KeplerTargetPixelFile('ktwo246864600-c13_lpd-targ.fits',quality_bitmask =1130799+32768)

qual.create_quality_mask(tpf.quality,1130799)

#Build new aperture
aper = tpf.pipeline_mask
aper[4][4]=True
aper[4][5]=True
tpf.plot(aperture_mask=aper, mask_color='red')

#Create custom light curve
lc_custom = tpf.to_lightcurve(aperture_mask = aper.astype(bool)).remove_nans()
lc_custom.plot()

#Create periodgram
pg = lc_custom.to_periodogram(oversample_factor=1)
pg.plot(view='period', scale='log')
pg = lc_custom.to_periodogram(minimum_period = 0.06*u.day,maximum_period = 0.07*u.day,oversample_factor=10)
lc_custom.fold(pg.period_at_max_power).scatter()